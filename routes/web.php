<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    //return "view('welcome')";
    return "Bienvenido";
});

Route::get('cursos',function(){
    return "Bienvenido a la página de cursos";
});
Route::get ('sesiones/{sesion}', function ($sesion) {
    return "Ingresaste a la sesión número: $sesion";
}) ;

Route::get ('alumnos/{carrera}/{asignatura?}', function ($carrera, $asignatura=null) {
if ($asignatura){
    return "Bienvenido a la carrera: $carrera, asignatura: $asignatura";
}
else {
    return "Bienvenido a la carrera: $carrera";
}   
});